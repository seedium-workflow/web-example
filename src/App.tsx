import { FC } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

// ** Store
import { store } from './modules/core/store';

// ** Context
import { AuthProvider } from 'modules/auth/providers/auth.provider';

// ** Constants
import { authRoutes } from 'modules/core/constants/routes/auth-routes.constants';

// ** Components
import { RouteRoot } from './modules/core/containers/route-root';
import Auth0Callback from 'pages/auth-pages/auth0-callback';

const App: FC = () => {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path={authRoutes.callback} component={Auth0Callback} />
          <Route
            render={() => (
              <AuthProvider>
                <RouteRoot />
              </AuthProvider>
            )}
          />
        </Switch>
        <ToastContainer newestOnTop />
      </Router>
    </Provider>
  );
};

export default App;
