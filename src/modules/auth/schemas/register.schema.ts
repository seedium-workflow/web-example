import * as yup from 'yup';

// ** Types
import { RegistrationFormValuesType } from 'modules/auth/types/auth0-register.types';

// ** Constants
import { MIN_PASSWORD } from 'modules/auth/constants/password.constant';

export const registerSchema: yup.SchemaOf<RegistrationFormValuesType> = yup
  .object()
  .shape({
    email: yup
      .string()
      .email('Invalid email')
      .required('Field cannot be empty'),
    password: yup
      .string()
      .required('Field cannot be empty')
      .min(MIN_PASSWORD, `Required more than ${MIN_PASSWORD} symbols`),
    username: yup.string().required('Field cannot be empty'),
    isAcceptedTerms: yup
      .bool()
      .oneOf([true], 'Privacy and policy need to be accepted')
      .required(),
  });
