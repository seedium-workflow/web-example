import * as yup from 'yup';

// ** Types
import { LoginCredentialsType } from 'modules/auth/types/login.types';

// ** Constants
import { MIN_PASSWORD } from 'modules/auth/constants/password.constant';

export const loginSchema: yup.SchemaOf<LoginCredentialsType> = yup
  .object()
  .shape({
    email: yup
      .string()
      .email('Invalid email')
      .required('Field cannot be empty'),
    password: yup
      .string()
      .required('Field cannot be empty')
      .min(MIN_PASSWORD, `Required more than ${MIN_PASSWORD} symbols`),
  });
