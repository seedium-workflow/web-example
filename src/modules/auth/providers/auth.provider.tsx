import { useEffect, useCallback, FC } from 'react';

// ** Context
import { AuthContext } from 'modules/auth/context/auth.context';

// ** Hooks
import { useBoolean } from 'modules/core/hooks/use-boolean';

// ** Components
import { Spinner } from 'modules/core/components';

// ** Utils
import { getFromLocalStorage } from 'modules/core/utils/storage.utils';

// ** Services
import { auth0Service } from 'modules/auth/services/auth0.services';

export const AuthProvider: FC = ({ children }) => {
  const [isAuthLoading, setIsAuthLoading] = useBoolean(true);
  const [isAuth, setIsAuth] = useBoolean(false);

  const isExpiredSession = useCallback(() => {
    const currentTime: number = Date.now();
    const expiresAt = getFromLocalStorage('expiresAt');
    const isRememberAuthorization = getFromLocalStorage(
      'isRememberAuthorization'
    );

    if (expiresAt === null) return false;

    if (currentTime > expiresAt && !isRememberAuthorization) {
      return true;
    }

    return false;
  }, []);

  const handleCheckSession = useCallback(async () => {
    if (isExpiredSession()) {
      setIsAuth.off();
    } else {
      try {
        await auth0Service.checkSession();

        setIsAuth.on();
      } catch (error) {
        setIsAuth.off();
      }
    }

    setIsAuthLoading.off();
  }, [setIsAuthLoading, isExpiredSession, setIsAuth]);

  useEffect(() => {
    handleCheckSession();
  }, [handleCheckSession]);

  if (isAuthLoading) {
    return <Spinner />;
  }

  return (
    <AuthContext.Provider value={{ isAuth }}>{children}</AuthContext.Provider>
  );
};
