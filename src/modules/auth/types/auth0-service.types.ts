type Auth0HashData = {
  accessToken: string;
  expiresIn: number;
};

type CheckSessionResult = {
  accessToken: string;
};

export const isAuth0HashData = (
  hashData: unknown
): hashData is Auth0HashData => {
  if (
    hashData &&
    typeof hashData === 'object' &&
    'accessToken' in hashData &&
    'expiresIn' in hashData
  ) {
    return true;
  }

  return false;
};

export const isCheckSessionResult = (
  result: CheckSessionResult
): result is CheckSessionResult => {
  if ('accessToken' in result) {
    return true;
  }

  return false;
};
