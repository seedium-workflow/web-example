import { createContext } from 'react';

type AuthContextValue = {
  isAuth: boolean;
};

export const AuthContext = createContext<AuthContextValue>({
  isAuth: false,
});
