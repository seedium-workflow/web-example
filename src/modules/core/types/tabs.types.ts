import { FC } from 'react';
import { Icon } from 'react-feather';

export type BaseTabTypes<T> = {
  tabName: string;
  tabIcon: Icon;
  Content: FC<T>;
  isAvailable: boolean;
};
