export type Color =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'info'
  | 'warning'
  | 'dark'
  | 'light-primary'
  | 'light-secondary'
  | 'light-success'
  | 'light-danger'
  | 'light-info'
  | 'light-warning'
  | 'light-dark';

export type NavbarType = 'static' | 'sticky' | 'floating' | 'hidden';
export type FooterType = 'static' | 'sticky' | 'hidden';

export type ThemeConfig = {
  app: { appName: string; appLogoImage: string };
  layout: {
    isRTL: boolean;
    skin: 'light' | 'dark' | 'bordered' | 'semi-dark';
    routerTransition: 'fadeIn' | 'fadeInLeft' | 'zoomIn' | 'none';
    type: 'vertical' | 'horizontal';
    contentWidth: 'full' | 'boxed';
    menu: {
      isHidden: boolean;
      isCollapsed: boolean;
    };
    navbar: {
      type: NavbarType;
      backgroundColor:
        | 'primary'
        | 'success'
        | 'secondary'
        | 'danger'
        | 'warning'
        | 'info'
        | 'light'
        | 'dark'
        | 'muted'
        | 'white';
    };
    footer: {
      type: FooterType;
    };
    scrollTop: boolean;
  };
};
