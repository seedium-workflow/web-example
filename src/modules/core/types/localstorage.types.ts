export type ILocalStorage = {
  token: string;
};

export type LocalStorageKeys = keyof ILocalStorage;

export interface ILocalStorageService {
  get<Key extends LocalStorageKeys>(key: Key): ILocalStorage[Key];
  add<Key extends LocalStorageKeys, Value extends ILocalStorage[Key]>(
    key: Key,
    value: Value
  ): void;
  remove<Key extends LocalStorageKeys>(key: Key): void;
}
