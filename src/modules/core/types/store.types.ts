// ** Types
import { CoreState } from 'modules/core/redux/core.types';

export type RootState = {
  layout: CoreState;
};
