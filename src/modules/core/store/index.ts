import { createStore, compose } from 'redux';
import { rootReducer } from './root-reducer';
import { createBrowserHistory } from 'history';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const history = createBrowserHistory();

// ** Dev Tools
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// ** Create store

const store = createStore(rootReducer, composeEnhancers());

export type AppDispatch = typeof store.dispatch;

export { store, history };
