import type { CoreActions } from 'modules/core/redux/core.types';

export type RootActionTypes = CoreActions;
