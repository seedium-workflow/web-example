import { combineReducers, Reducer } from 'redux';

// ** Types
import type { RootState } from 'modules/core/types/store.types';
import type { RootActionTypes } from 'modules/core/store/root-actions';

// ** Reducers
import { coreReducer } from 'modules/core/redux/core.reducers';

export const rootReducer: Reducer<RootState, RootActionTypes> =
  combineReducers<RootState>({
    layout: coreReducer,
  });
