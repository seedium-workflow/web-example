export type PromptContainerProps = {
  isDirtyForm: boolean;
  excludedRoute?: string;
};
