import { format, fromUnixTime } from 'date-fns';

export const formatDate = (date: Date, formatType: string): string => {
  return format(date, formatType);
};

export const formatUnixTime = (date: number, formatType: string): string => {
  return format(fromUnixTime(date), formatType);
};
