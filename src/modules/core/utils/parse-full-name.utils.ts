export const parseFullName = (
  fullName: string | undefined
): { first_name: string; last_name: string } => {
  if (fullName === undefined) return { first_name: '', last_name: '' };
  const [first_name, ...last_name] = fullName.split(' ');

  return {
    first_name,
    last_name: last_name.join(' ').trim(),
  };
};
