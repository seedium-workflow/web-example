import { useState, useCallback } from 'react';

export type UseFormStepper = () => {
  availableTabs: number[];
  step: number;
  handleMoveToNextStep: (direction: 1 | -1) => void;
  handleSetStep: (nextStep: number) => void;
  initAvailableTabs: (nextAvailableLength: number) => void;
};

export const useFormStepper: UseFormStepper = () => {
  const [step, setStep] = useState<number>(0);
  const [availableTabs, setAvailableTabs] = useState<number[]>([]);

  const handleMoveToNextStep = useCallback(
    (direction: 1 | -1) => {
      setStep((prevStep) => prevStep + direction);

      const nextStep = step + direction;

      if (!availableTabs.includes(nextStep)) {
        const newAvailableTabs: number[] = [...availableTabs, nextStep];

        setAvailableTabs(newAvailableTabs);
      }
    },
    [availableTabs, step]
  );

  const handleSetStep = useCallback((nextStep: number) => {
    setStep(nextStep);
  }, []);

  const initAvailableTabs = useCallback((nextAvailableLength: number) => {
    const newAvailableTabs: number[] = [];

    const array = Array.from({ length: nextAvailableLength }, (_, i) => i);

    array.forEach((_, index) => {
      newAvailableTabs.push(index);
    });

    setAvailableTabs(newAvailableTabs);
  }, []);

  return {
    step,
    availableTabs,
    handleMoveToNextStep,
    handleSetStep,
    initAvailableTabs,
  };
};
