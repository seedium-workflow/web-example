import { useCallback } from 'react';
import { useHistory } from 'react-router-dom';

export const useHistoryWithState = () => {
  const history = useHistory();

  const pushWithState = useCallback(
    <State>(route: string, state: State): void =>
      history.push(route, state || {}),
    [history]
  );

  return { history, pushWithState } as const;
};
