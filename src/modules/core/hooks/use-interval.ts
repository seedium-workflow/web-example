import { useRef, useCallback, useEffect, MutableRefObject } from 'react';

// ** Constants
import { SECOND_IN_MILLISECONDS } from 'modules/core/constants/time';

type Start = (callback: () => void, delay?: number) => void;
type Clear = () => void;

export const useInterval = (): { start: Start; clear: Clear } => {
  const interval: MutableRefObject<number | null> = useRef(null);

  const start: Start = useCallback(
    (callback, delay = SECOND_IN_MILLISECONDS) => {
      if (interval.current) {
        window.clearInterval(interval.current);
      }

      interval.current = window.setInterval(callback, delay);
    },
    []
  );

  const clear: Clear = useCallback(() => {
    if (interval.current) {
      window.clearInterval(interval.current);
    }
  }, []);

  useEffect(() => {
    return () => {
      if (interval.current) {
        window.clearInterval(interval.current);
      }
    };
  }, []);

  return { start, clear };
};
