import { useDispatch, TypedUseSelectorHook, useSelector } from 'react-redux';

// ** Types
import type { AppDispatch } from 'modules/core/store';
import type { RootState } from 'modules/core/types/store.types';

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
