import { useEffect, useState, useCallback } from 'react';

export const useWindowSize = () => {
  const [width, setWidth] = useState<number>(window.innerWidth);
  const [height, setHeight] = useState<number>(window.innerHeight);

  const handleSetResolution = useCallback(() => {
    setWidth(window.innerWidth);
    setHeight(window.innerHeight);
  }, []);

  useEffect(() => {
    window.addEventListener('resize', handleSetResolution);

    return () => window.removeEventListener('resize', handleSetResolution);
  }, [handleSetResolution]);

  return [width, height] as const;
};
