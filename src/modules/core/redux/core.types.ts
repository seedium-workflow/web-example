import { ActionType } from 'typesafe-actions';
import * as actions from 'modules/core/redux/core.actions';
import { NavbarType, FooterType } from 'modules/core/types/theme.types';

export type CoreState = Readonly<{
  skin: string;
  menuCollapsed: boolean;
  menuHidden: boolean;
  contentWidth: string;
  navbarType: NavbarType;
  footerType: FooterType;
}>;

export type CoreActions = ActionType<typeof actions>;
