import { FC } from 'react';

// ** Components
import { NavBar, Menu, BaseLayout } from 'modules/core/layouts';

export const MainLayout: FC = ({ children }) => {
  return (
    <BaseLayout
      menu={(props) => <Menu {...props} />}
      navbar={(props) => <NavBar {...props} />}
      footer={() => null}
    >
      {children}
    </BaseLayout>
  );
};
