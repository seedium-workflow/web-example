export type NavbarProps = {
  handleSetMenuVisibility: (visibility: boolean) => void;
};
