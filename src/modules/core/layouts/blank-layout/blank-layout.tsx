// ** React Imports
import React from 'react';

export const BlankLayout: React.FC = ({ children }) => {
  return (
    <div className='blank-page'>
      <div className='app-content content'>
        <div className='content-wrapper'>
          <div className='content-body'>{children}</div>
        </div>
      </div>
    </div>
  );
};
