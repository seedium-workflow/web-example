import { FC } from 'react';

// ** Types
import { FooterProps } from './footer.interfaces';

export const Footer: FC<FooterProps> = () => {
  return (
    <p className='clearfix mb-0 text-center'>
      <span className=' d-block d-md-inline-block mt-25 '>
        COPYRIGHT © {new Date().getFullYear()}{' '}
        <a href='#' target='_blank' rel='noopener noreferrer'>
          Open Culture
        </a>
      </span>
    </p>
  );
};
