import { ReactNode } from 'react';

import { MenuProps } from 'modules/core/layouts/menu/menu.interfaces';

export type SidebarProps = {
  menu: ((props: MenuProps) => ReactNode) | null;
  menuCollapsed: boolean;
  handleMenuCollapsed: (collapsed: boolean) => void;
  handleSetMenuVisibility: (visibility: boolean) => void;
  menuVisibility: boolean;
};
