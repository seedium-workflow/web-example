import { ReactNode } from 'react';

// ** Types
import { FooterProps } from 'modules/core/layouts/footer/footer.interfaces';
import { NavbarProps } from 'modules/core/layouts/navbar/navbar.interfaces';
import { MenuProps } from 'modules/core/layouts/menu/menu.interfaces';

export type BaseLayoutProps = {
  navbar?: (props: NavbarProps) => ReactNode;
  menu?: (props: MenuProps) => ReactNode;
  footer?: ((props: FooterProps) => ReactNode) | null;
};
