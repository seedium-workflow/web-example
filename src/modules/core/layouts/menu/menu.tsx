// ** React Imports
import { Fragment, FC } from 'react';

// ** Constants
import { generatePageNavigation } from 'modules/core/constants/navigation.constants';

// ** Third Party Components
import PerfectScrollbar from 'react-perfect-scrollbar';

// ** Components
import { MenuHeader, MenuList } from 'modules/core/components';

// ** Types
import { MenuProps } from './menu.interfaces';

//  ** Styles
import './menu.scss';

export const Menu: FC<MenuProps> = (props) => {
  // ** Props
  const { menuHover, shadowRef, scrollMenu } = props;

  return (
    <Fragment>
      <MenuHeader {...props} />
      <div className='shadow-bottom' ref={shadowRef} />
      <PerfectScrollbar
        className='main-menu-content d-flex'
        options={{ wheelPropagation: false }}
        onScrollY={(container) => scrollMenu(container)}
      >
        <div className='w-100'>
          <ul className='d-flex flex-column navigation navigation-main mt-3 menuList'>
            <MenuList list={generatePageNavigation()} menuHover={menuHover} />
          </ul>
        </div>
      </PerfectScrollbar>
    </Fragment>
  );
};
