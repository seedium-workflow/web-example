import { RefObject } from 'react';

export type MenuProps = {
  scrollMenu: (container: HTMLElement) => void;
  menuHover: boolean;
  shadowRef: RefObject<HTMLDivElement>;
  menuCollapsed: boolean;
  handleMenuCollapsed: (collapsed: boolean) => void;
  handleSetMenuVisibility: (visibility: boolean) => void;
  menuVisibility: boolean;
};
