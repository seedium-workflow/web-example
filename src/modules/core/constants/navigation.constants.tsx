import { Globe } from 'react-feather';

// ** Types
import type { NavigationType } from 'modules/core/types/navigation.types';

// ** Constants
import { protectedRoutes } from 'modules/core/constants/routes/protected-routes.constants';

export const generatePageNavigation = (): NavigationType => ({
  menuTitle: '',
  items: [
    {
      title: 'Dashboard',
      path: protectedRoutes.dashboard,
      icon: <Globe size={12} />,
      isAvailable: true,
    },
  ],
});
