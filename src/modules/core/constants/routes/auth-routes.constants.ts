import type { RouteType } from 'modules/core/types/routes.types';
import {
  Login,
  Register,
  ForgotPassword,
  ResetPassword,
  VerifyEmail,
} from 'pages';

type AuthRoutesType =
  | 'authRoot'
  | 'login'
  | 'register'
  | 'resetPassword'
  | 'forgotPassword'
  | 'callback'
  | 'verifyEmail';

export const authRoutes: Record<AuthRoutesType, string> = {
  authRoot: '/auth',
  login: '/auth/login',
  register: '/auth/register',
  forgotPassword: '/auth/forgot-password',
  resetPassword: '/auth/reset-password',
  callback: '/auth/callback',
  verifyEmail: '/auth/verify-email',
};

export const authRoutesMap: RouteType[] = [
  {
    path: authRoutes.login,
    layout: 'BlankLayout',
    component: Login,
    exact: true,
  },
  {
    path: authRoutes.register,
    layout: 'BlankLayout',
    component: Register,
    exact: true,
  },
  {
    path: authRoutes.forgotPassword,
    layout: 'BlankLayout',
    component: ForgotPassword,
    exact: true,
  },
  {
    path: authRoutes.resetPassword,
    layout: 'BlankLayout',
    component: ResetPassword,
    exact: true,
  },
  {
    path: authRoutes.verifyEmail,
    layout: 'BlankLayout',
    component: VerifyEmail,
    exact: true,
  },
];
