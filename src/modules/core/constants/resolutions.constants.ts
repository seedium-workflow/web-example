type Resolutions = 'xs' | 'sm' | 'md' | 'lg' | 'xl' | 'xxl';

export const resolutionsMap: Record<Resolutions, number> = {
  xs: 0,
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
  xxl: 1440,
};
