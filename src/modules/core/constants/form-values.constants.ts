export const MAX_TEXTAREA_VALUE: number = 1000;

export const MIN_PHONE_LENGTH = 10;

export const MAX_PHONE_LENGTH = 13;
