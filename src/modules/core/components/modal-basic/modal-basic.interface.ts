import { ReactNode } from 'react';
import { ModalBodyProps, ModalHeaderProps, ModalProps } from 'reactstrap';

export type ModalBasicProps = {
  isOpen: boolean;
  onClose: () => void;
  onConfirm?: () => void;
  children: ReactNode;
  header?: string | ReactNode;
  closeButtonText?: string;
  confirmButtonText?: string;
  headerProps?: ModalHeaderProps;
  bodyProps?: ModalBodyProps;
} & ModalProps;
