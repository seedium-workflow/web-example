import { FC } from 'react';
import classnames from 'classnames';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

// ** Types
import { ModalBasicProps } from './modal-basic.interface';

export const ModalBasic: FC<ModalBasicProps> = ({
  isOpen,
  onClose,
  onConfirm,
  header,
  children,
  closeButtonText = 'Close',
  confirmButtonText = 'Accept',
  headerProps,
  bodyProps,
  size = 'modal-md',
  scrollable,
  ...props
}) => {
  return (
    <Modal
      isOpen={isOpen}
      toggle={onClose}
      className={classnames(
        scrollable ? 'modal-dialog-scrollable' : 'modal-dialog-centered',
        size
      )}
      {...props}
    >
      <ModalHeader
        className={classnames({ 'bg-transparent': !header })}
        toggle={onClose}
        {...headerProps}
      >
        {header}
      </ModalHeader>
      <ModalBody {...bodyProps}>{children}</ModalBody>
      {(closeButtonText || confirmButtonText) && (
        <ModalFooter>
          {closeButtonText && (
            <Button color='danger' onClick={onClose}>
              {closeButtonText}
            </Button>
          )}
          {confirmButtonText && (
            <Button color='primary' onClick={onConfirm}>
              {confirmButtonText}
            </Button>
          )}
        </ModalFooter>
      )}
    </Modal>
  );
};
