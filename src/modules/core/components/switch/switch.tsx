import { FC, forwardRef } from 'react';
import classnames from 'classnames';
import { CustomInput } from 'reactstrap';

import type { SwitchProps } from './switch.interface';
import { Check, X } from 'react-feather';

const Label: FC = () => (
  <>
    <span className='switch-icon-left'>
      <Check size={14} />
    </span>
    <span className='switch-icon-right'>
      <X size={14} />
    </span>
  </>
);

export const Switch = forwardRef<HTMLInputElement, SwitchProps>(
  ({ color, withIcons, className, ...props }, ref) => {
    return (
      <CustomInput
        type='switch'
        innerRef={ref}
        label={withIcons && <Label />}
        className={classnames(
          { [`custom-control-${color}`]: color },
          'mr-0',
          className
        )}
        inline
        {...props}
      />
    );
  }
);
