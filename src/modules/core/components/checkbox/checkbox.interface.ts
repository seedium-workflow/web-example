import { ChangeEvent, CSSProperties } from 'react';

export type CheckboxProps = {
  id: string;
  name?: string;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  onClick?: () => void;
  label?: string;
  color?: 'secondary' | 'success' | 'danger' | 'warning' | 'info';
  defaultChecked?: boolean;
  checked: boolean;
  disabled?: boolean;
  className?: string;
  invalid?: boolean;
  style?: CSSProperties;
};
