import { forwardRef } from 'react';
import { CustomInput } from 'reactstrap';
import classnames from 'classnames';

import type { CheckboxProps } from './checkbox.interface';

export const Checkbox = forwardRef<HTMLInputElement, CheckboxProps>(
  ({ color, className, onClick, ...props }, ref) => {
    return (
      <div onClick={onClick}>
        <CustomInput
          type='checkbox'
          className={classnames(className, {
            [`custom-control-${color}`]: color,
          })}
          inline
          {...props}
          innerRef={ref}
        />
      </div>
    );
  }
);
