import { ReactNode } from 'react';
import {
  DropdownMenuProps,
  DropdownToggleProps,
  DropdownProps as DefaultDropdownProps,
} from 'reactstrap';
import { Icon } from 'react-feather';

type DropdownSettings = {
  disabled?: boolean;
  divider?: boolean;
  className?: string;
  iconClassName?: string;
};

interface IDropdownItem {
  settings?: DropdownSettings;
  content: string | ReactNode;
  icon?: Icon;
  onClick: () => void;
  isAvailable: boolean;
}

export type DropdownProps = {
  toggleSettings?: DropdownToggleProps;
  menuSettings?: DropdownMenuProps;
  defaultValue?: string | ReactNode;
  defaultIcon?: Icon;
  dropdownProps?: DefaultDropdownProps;
  dropdownList: IDropdownItem[];
};
