// ** React Imports
import React from 'react';
import { useLocation, NavLink } from 'react-router-dom';

// ** Third Party Components
import { Badge } from 'reactstrap';
import classnames from 'classnames';

import './menu-link.scss';

export const MenuLink: React.FC<any> = ({ item }) => {
  const location = useLocation();
  const currentURL = location.pathname;

  return (
    <li
      className={classnames(
        {
          'nav-item': !item.children,
          disabled: item.disabled,
          active: currentURL.includes(item.path),
        },
        'menuLink'
      )}
    >
      <NavLink className='d-flex align-items-center' to={item.path}>
        {item.icon}
        <span className='menu-item text-truncate'>{item.title}</span>

        {item.badge && item.badgeText ? (
          <Badge className='ml-auto mr-1' color={item.badge} pill>
            {item.badgeText}
          </Badge>
        ) : null}
      </NavLink>
    </li>
  );
};
