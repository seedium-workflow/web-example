import React from 'react';
import classnames from 'classnames';

// ** Types
import type { SpinnerProps } from './spinner.interfaces';

// ** Styles
import styles from './spinner.module.scss';

export const Spinner: React.FC<SpinnerProps> = ({ className }) => {
  return (
    <div className={classnames(styles.wrapper, className)}>
      <div className={classnames('loading', styles.spinnerLoading)}>
        <div className='effect-1 effects' />
        <div className='effect-2 effects' />
        <div className='effect-3 effects' />
      </div>
    </div>
  );
};
