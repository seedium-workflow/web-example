import { Icon } from 'react-feather';

export type TooltipProps = {
  id: string;
  className?: string;
  text: string;
  Icon?: Icon | null;
  iconSize?: number;
};
