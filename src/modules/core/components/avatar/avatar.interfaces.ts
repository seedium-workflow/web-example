import React, { CSSProperties } from 'react';

import { Color } from 'modules/core/types/theme.types';

export type AvatarProps = {
  imgClassName?: string;
  className?: string;
  img?: string;
  imgHeight?: string;
  imgWidth?: string;
  status?: 'online' | 'offline' | 'away' | 'busy';
  id?: string;
  name?: string;
  // eslint-disable-next-line no-undef
  tag?: keyof JSX.IntrinsicElements;
  badgeUp?: boolean;
  content?: string;
  icon?: React.ReactNode;
  contentStyles?: CSSProperties;
  badgeText?: string;
  size?: 'sm' | 'lg' | 'xl';
  badgeColor?:
    | 'primary'
    | 'secondary'
    | 'success'
    | 'danger'
    | 'info'
    | 'warning'
    | 'dark'
    | 'light-primary'
    | 'light-secondary'
    | 'light-success'
    | 'light-danger'
    | 'light-info'
    | 'light-warning'
    | 'light-dark';
  color?: Color;
  initials?: boolean;
};
