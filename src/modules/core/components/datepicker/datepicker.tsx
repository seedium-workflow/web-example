import React from 'react';
import classnames from 'classnames';
import { Label } from 'reactstrap';
import Flatpickr from 'react-flatpickr';

// ** Types
import { DatePickerProps } from './datepicker.interfaces';

// ** Styles
import '@core/scss/react/libs/flatpickr/flatpickr.scss';
import styles from './datepicker.module.scss';

export const DatePicker: React.FC<DatePickerProps> = ({
  value,
  onChange,
  label,
  ...props
}) => {
  return (
    <>
      <Label for={label}>{label}</Label>
      <Flatpickr
        className={classnames('form-control', styles.formControl)}
        value={value}
        onChange={(date) => onChange(date)}
        id={label}
        {...props}
      />
    </>
  );
};
