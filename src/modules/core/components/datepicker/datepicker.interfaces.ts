import { DateTimePickerProps } from 'react-flatpickr';

export type DatePickerProps = {
  value: Date;
  onChange: (date: Date[]) => void;
  label: string;
} & DateTimePickerProps;
