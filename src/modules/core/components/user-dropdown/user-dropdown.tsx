// ** React Imports
import { FC, useCallback, MouseEvent, useContext } from 'react';
import { Link } from 'react-router-dom';

// ** Context
import { AuthContext } from 'modules/auth/context/auth.context';

// ** Custom Components
import { Avatar } from 'modules/core/components';

// ** Services
import { auth0Service } from 'modules/auth/services/auth0.services';

// ** Constants
import { protectedRoutes } from 'modules/core/constants/routes/protected-routes.constants';
import { authRoutes } from 'modules/core/constants/routes/auth-routes.constants';

// ** Third Party Components
import {
  UncontrolledDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
} from 'reactstrap';
import { User, Power, LogOut, MoreHorizontal } from 'react-feather';

// ** Styles
import styles from './user-dropdown.module.scss';

export const UserDropdown: FC = () => {
  const { isAuth } = useContext(AuthContext);

  const handleLogout = useCallback(() => {
    auth0Service.logout({});
  }, []);

  return (
    <UncontrolledDropdown tag='li' className='dropdown-user nav-item'>
      <DropdownToggle
        href='/'
        tag='a'
        className='nav-link dropdown-user-link'
        onClick={(e: MouseEvent) => e.preventDefault()}
      >
        {isAuth ? (
          <>
            <Avatar img={''} imgHeight='40px' imgWidth='40px' status='online' />
          </>
        ) : (
          <div className={styles.commands}>
            <MoreHorizontal size={20} />
          </div>
        )}
      </DropdownToggle>
      <DropdownMenu right>
        {isAuth ? (
          <>
            <DropdownItem tag={Link} to={protectedRoutes.dashboard}>
              <User size={14} className='mr-75' />
              <span className='align-middle'>Profile</span>
            </DropdownItem>

            <DropdownItem tag='div' onClick={handleLogout}>
              <Power size={14} className='mr-75' />
              <span className='align-middle'>Logout</span>
            </DropdownItem>
          </>
        ) : (
          <DropdownItem tag={Link} to={authRoutes.login}>
            <LogOut size={14} className='mr-75' />
            <span className='align-middle'>Login & Sign Up</span>
          </DropdownItem>
        )}
      </DropdownMenu>
    </UncontrolledDropdown>
  );
};

export default UserDropdown;
