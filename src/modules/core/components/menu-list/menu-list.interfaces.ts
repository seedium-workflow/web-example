import type { NavigationType } from 'modules/core/types/navigation.types';

export type MenuListProps = { list: NavigationType; menuHover?: boolean };
