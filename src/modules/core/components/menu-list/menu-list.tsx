import React from 'react';
import classnames from 'classnames';
import { MoreHorizontal } from 'react-feather';
// ** Components
import { MenuLink } from 'modules/core/components';

// ** Types
import { MenuListProps } from './menu-list.interfaces';

// ** Styles
import './menu-list.scss';

export const MenuList: React.FC<MenuListProps> = ({ list, ...restProps }) => {
  return (
    <>
      {list.menuTitle && (
        <>
          <li className={classnames('menuTitle')}>{list.menuTitle}</li>
          <li className={classnames('menuDots')}>
            <MoreHorizontal size={15} />
          </li>
        </>
      )}
      {list.items.map((item) =>
        item.isAvailable ? (
          <MenuLink key={item.title} item={item} {...restProps} />
        ) : null
      )}
    </>
  );
};
