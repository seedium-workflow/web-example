import type { TooltipProps } from 'modules/core/components/tooltip/tooltip.interfaces';

export interface ControlledTooltipProps extends TooltipProps {
  isOpen: boolean;
}
