// ** React Imports
import React, { Fragment, useState, forwardRef } from 'react';

// ** Third Party Components
import classnames from 'classnames';
import { Eye, EyeOff } from 'react-feather';
import {
  InputGroup,
  InputGroupAddon,
  Input,
  InputGroupText,
  Label,
} from 'reactstrap';

// ** Types
import type { InputPasswordToggleProps } from './input-password-toggle.interfaces';

// ** Styles
import styles from './input-password-toggle.module.scss';

const defaultSize = 14;

export const InputPasswordToggle = forwardRef<
  HTMLInputElement,
  InputPasswordToggleProps
>((props, ref) => {
  // ** Props
  const {
    label,
    hideIcon,
    showIcon,
    visible = false,
    className = 'input-group-merge',
    htmlFor,
    placeholder,
    iconSize,
    inputClassName = '',
    invalid,
    feedbackText,
    ...rest
  } = props;

  // ** State
  const [inputVisibility, setInputVisibility] = useState<boolean>(visible);

  // ** Renders Icon Based On Visibility
  const renderIcon = (): React.ReactNode => {
    const size = iconSize ? iconSize : defaultSize;

    if (!inputVisibility) {
      return hideIcon ? hideIcon : <Eye size={size} />;
    }

    return showIcon ? showIcon : <EyeOff size={size} />;
  };

  return (
    <Fragment>
      {label ? <Label for={htmlFor}>{label}</Label> : null}
      <InputGroup
        className={classnames({
          [className]: className,
        })}
      >
        <Input
          type={!inputVisibility ? 'password' : 'text'}
          placeholder={placeholder ? placeholder : '············'}
          className={classnames({
            [inputClassName]: inputClassName,
          })}
          innerRef={ref}
          invalid={invalid}
          {...(label && htmlFor
            ? {
                id: htmlFor,
              }
            : {})}
          {...rest}
        />

        <InputGroupAddon
          addonType='append'
          onClick={() => setInputVisibility(!inputVisibility)}
        >
          <InputGroupText className='cursor-pointer'>
            {renderIcon()}
          </InputGroupText>
        </InputGroupAddon>
      </InputGroup>
      {invalid && feedbackText && (
        <p className={styles.error}>{feedbackText}</p>
      )}
    </Fragment>
  );
});
