export type ColorPickerProps = {
  color: string;
  onChange: (color: string) => void;
};
