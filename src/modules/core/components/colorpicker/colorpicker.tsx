import React, { useRef, useCallback } from 'react';
import { SketchPicker, ColorResult } from 'react-color';

// ** Types
import { ColorPickerProps } from './colorpicker.interfaces';

// ** Hooks
import { useBoolean } from 'modules/core/hooks/use-boolean';
import { useOnClickOutside } from 'modules/core/hooks/use-click-outside';

// ** Styles
import styles from './colorpicker.module.scss';

export const ColorPicker: React.FC<ColorPickerProps> = ({
  color = 'rgba(0,0,0)',
  onChange,
}) => {
  const [isOpen, setIsOpen] = useBoolean(false);
  const colorRef = useRef<HTMLDivElement>(null);

  useOnClickOutside(colorRef, setIsOpen.off);

  const handleChange = useCallback(
    (color: ColorResult) => {
      onChange(color.hex);
    },
    [onChange]
  );

  return (
    <label className={styles.label} onClick={() => setIsOpen.on()}>
      <div
        ref={colorRef}
        className={styles.input}
        style={{ backgroundColor: color }}
      >
        {isOpen && (
          <SketchPicker
            className={styles.picker}
            color={color}
            onChange={handleChange}
          />
        )}
      </div>

      <div className={styles.value}>{color || 'Choose color'}</div>
    </label>
  );
};
