import { Props } from 'react-select';
import { HTMLInputSize } from 'modules/core/types/input.types';

export type Option = { label: string; value: string | number };

export type SelectProps = {
  id: string;
  wrapperClassName?: string;
  options: Option[];
  label?: string;
  disabled?: boolean;
  bsSize?: HTMLInputSize;
  feedbackText?: string;
  invalid?: boolean;
} & Props<Option>;
