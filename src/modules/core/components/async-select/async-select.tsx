import { FC } from 'react';
import AsyncSelect from 'react-select/async';
import { FormGroup, Label, FormFeedback } from 'reactstrap';
import classnames from 'classnames';

// ** Utils
import { selectThemeColors } from 'modules/core/utils/select-theme-colors';

// ** Types
import { SelectAsyncProps } from './async-select.interface';

export const SelectAsync: FC<SelectAsyncProps> = ({
  name,
  wrapperClassName,
  label,
  feedbackText,
  invalid,
  ...props
}) => {
  return (
    <FormGroup className={wrapperClassName}>
      {label && <Label>{label}</Label>}

      <AsyncSelect
        className={classnames('react-select', { 'is-invalid': invalid })}
        classNamePrefix='select'
        name={name || 'callback-react-select'}
        defaultOptions
        theme={selectThemeColors}
        {...props}
      />
      {invalid && feedbackText && (
        <FormFeedback valid={!invalid}>{feedbackText}</FormFeedback>
      )}
    </FormGroup>
  );
};
