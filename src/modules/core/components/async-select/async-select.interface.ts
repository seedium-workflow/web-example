import { OnChangeValue } from 'react-select';
import { Option } from 'modules/core/components/select/select.interface';

export type SelectAsyncProps = {
  id: string;
  onChange: (newValue: OnChangeValue<Option, boolean>) => void;
  isDisabled?: boolean;
  name?: string;
  wrapperClassName?: string;
  label?: string;
  feedbackText?: string;
  invalid?: boolean;
};
