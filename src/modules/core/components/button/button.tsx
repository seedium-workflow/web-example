// ** React Imports
import React, { useState, useEffect } from 'react';

// ** Third Party Components
import { Button as StrapButton } from 'reactstrap';
import classnames from 'classnames';

// ** Types
import { CustomButtonProps } from './button.interface';

// ** Styles
import styles from './button.module.scss';

const timeout = 500;

export const Button: React.FC<CustomButtonProps> = ({
  className,
  children,
  onClick,
  leftAddon = null,
  ...rest
}) => {
  // ** States
  const [mounted, setMounted] = useState(false);
  const [isRippling, setIsRippling] = useState(false);
  const [coords, setCoords] = useState({ x: -1, y: -1 });

  // ** Toggle mounted on mount & unmount
  useEffect(() => {
    setMounted(true);

    return () => setMounted(false);
  }, []);

  // ** Check for coords and set ripple
  useEffect(() => {
    if (mounted) {
      if (coords.x !== -1 && coords.y !== -1) {
        setIsRippling(true);
        setTimeout(() => setIsRippling(false), timeout);
      } else {
        setIsRippling(false);
      }
    }
  }, [coords, mounted]);

  // ** Reset Coords on ripple end
  useEffect(() => {
    if (mounted) {
      if (!isRippling) setCoords({ x: -1, y: -1 });
    }
  }, [isRippling, mounted]);

  return (
    <StrapButton
      className={classnames(styles['waves-effect'], className)}
      onClick={(e) => {
        const target = e.target as HTMLButtonElement;
        const rect = target.getBoundingClientRect();

        setCoords({ x: e.clientX - rect.left, y: e.clientY - rect.top });
        if (onClick) {
          onClick(e);
        }
      }}
      {...rest}
    >
      {leftAddon && <span className={styles.leftAddon}>{leftAddon}</span>}
      {children}
      {isRippling ? (
        <span
          className={styles['waves-ripple']}
          style={{
            left: coords.x,
            top: coords.y,
          }}
        />
      ) : null}
    </StrapButton>
  );
};
