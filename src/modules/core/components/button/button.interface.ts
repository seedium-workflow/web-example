import React from 'react';
import { ButtonProps } from 'reactstrap';

export type CustomButtonProps = {
  leftAddon?: React.ReactNode;
} & ButtonProps;
