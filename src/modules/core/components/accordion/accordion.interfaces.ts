import React from 'react';

export type AccordionItem = {
  title: string;
  content: React.ReactNode;
  className?: string;
};

export type AccordionProps = {
  data: AccordionItem[];
  type?: 'shadow' | 'border' | 'margin';
  isSingleOpening?: boolean;
  active: number[];
  rootClassName?: string;
};
