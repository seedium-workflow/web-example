import { FC } from 'react';
import Select from 'react-select';
import CreatableSelect from 'react-select/creatable';

// ** Utils
import { selectThemeColors } from 'modules/core/utils/select-theme-colors';

import { BaseSelectProps } from './base-select.interface';

export const BaseSelect: FC<BaseSelectProps> = ({
  options,
  isCreatable,
  ...props
}) => {
  return (
    <>
      {isCreatable ? (
        <CreatableSelect
          theme={selectThemeColors}
          className='react-select'
          classNamePrefix='select'
          options={options}
          {...props}
        />
      ) : (
        <Select
          theme={selectThemeColors}
          className='react-select'
          classNamePrefix='select'
          options={options}
          {...props}
        />
      )}
    </>
  );
};
