import { FC } from 'react';
import classnames from 'classnames';

// ** Config
import themeConfig from 'modules/core/configs/theme-config';

// ** Types
import { AnimatedWrapperProps } from './animated-wrapper.interfaces';

export const AnimatedWrapper: FC<AnimatedWrapperProps> = ({
  children,
  classes,
}) => (
  <div
    className={classnames(
      `content-wrapper animate__animated animate__${themeConfig.layout.routerTransition}`,
      { [classes?.wrapper as string]: classes && classes.wrapper }
    )}
  >
    {children}
  </div>
);
