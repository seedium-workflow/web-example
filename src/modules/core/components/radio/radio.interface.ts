import { ChangeEvent } from 'react';

export type RadioProps = {
  id: string;
  name: string;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
  label?: string;
  color?: 'secondary' | 'success' | 'danger' | 'warning' | 'info';
  defaultChecked?: boolean;
  disabled?: boolean;
};
