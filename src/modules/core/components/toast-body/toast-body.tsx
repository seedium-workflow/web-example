import { FC } from 'react';
import classNames from 'classnames';
// components
import { Avatar } from 'modules/core/components/';

import type { ToastBodyProps } from './toast-body.interface';

export const ToastBody: FC<ToastBodyProps> = ({
  color = 'success',
  icon,
  text,
  textClassName,
}) => {
  return (
    <div className='toastify-header pb-0'>
      <div className='title-wrapper'>
        {icon && <Avatar size='sm' color={color} icon={icon} />}
        <h6 className={classNames('toast-title', textClassName)}>{text}</h6>
      </div>
    </div>
  );
};
