import { ChangeEvent, forwardRef, memo, useCallback } from 'react';
import classnames from 'classnames';
import { FormGroup, Label, Input, FormFeedback } from 'reactstrap';

import type { InputTextProps } from './input-text.interfaces';

// ** Constants
import { MAX_TEXTAREA_VALUE } from 'modules/core/constants/form-values.constants';

// ** Styles
import styles from './input-text.module.scss';

export const InputText = memo(
  forwardRef<HTMLInputElement | HTMLTextAreaElement, InputTextProps>(
    (
      {
        label,
        id,
        feedbackText,
        valid,
        invalid,
        type,
        rows = '8',
        actionButton,
        onChange,
        value,
        maxSymbols = MAX_TEXTAREA_VALUE,
        allowMaxSymbols = true,
        classes,
        ...props
      },
      ref
    ) => {
      const handleTextareaChange = useCallback(
        (e: ChangeEvent<HTMLInputElement>) => {
          if (e.target.value.length > maxSymbols && allowMaxSymbols) {
            return;
          }

          if (onChange) {
            onChange(e);
          }
        },
        [allowMaxSymbols, maxSymbols, onChange]
      );

      return (
        <FormGroup className={classnames('w-100', classes?.root)}>
          <div className='d-flex align-items-end w-100'>
            <div className='d-flex flex-wrap w-100 position-relative'>
              {label && (
                <Label for={id} className={classnames(classes?.label, 'w-100')}>
                  {label}
                </Label>
              )}
              {type === 'textarea' ? (
                <>
                  <Input
                    id={id}
                    type={type}
                    innerRef={ref}
                    valid={valid}
                    invalid={invalid}
                    rows={rows}
                    style={{ resize: 'none' }}
                    onChange={handleTextareaChange}
                    value={value}
                    {...props}
                  />
                  {allowMaxSymbols && (
                    <span className={styles.symbolCounter}>{`${
                      value?.length || 0
                    }/${maxSymbols}`}</span>
                  )}
                </>
              ) : (
                <Input
                  id={id}
                  type={type}
                  innerRef={ref}
                  valid={valid}
                  invalid={invalid}
                  onChange={onChange}
                  {...props}
                />
              )}
              {(valid || invalid) && feedbackText && (
                <FormFeedback valid={valid}>{feedbackText}</FormFeedback>
              )}
            </div>
            {actionButton && (
              <div className='flex-shrink-0'>{actionButton}</div>
            )}
          </div>
        </FormGroup>
      );
    }
  )
);
