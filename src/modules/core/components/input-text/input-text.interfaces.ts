import { ChangeEvent, CSSProperties, ReactNode } from 'react';
import { Classes } from 'modules/core/types/classes.types';
import { HTMLInputSize } from 'modules/core/types/input.types';

type HTMLInputType = 'text' | 'email' | 'textarea';

export type InputTextProps = {
  id: string;
  value?: string;
  onChange?: (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
  className?: string;
  name?: string;
  label?: string;
  type?: HTMLInputType;
  placeholder?: string;
  disabled?: boolean;
  readOnly?: boolean;
  bsSize?: HTMLInputSize;
  feedbackText?: string;
  valid?: boolean;
  invalid?: boolean;
  rows?: string;
  style?: CSSProperties;
  actionButton?: ReactNode;
  maxSymbols?: number;
  allowMaxSymbols?: boolean;
  classes?: Classes<'root' | 'label'>;
};
