import { FC, Suspense } from 'react';
import { Switch, Route, Redirect, RouteComponentProps } from 'react-router-dom';

// ** Components
import { Spinner } from 'modules/core/components';

// ** Layouts
import { LayoutWrapper } from 'modules/core/layouts';

// ** Utils
import { getRoutesAndPaths } from 'modules/core/utils/get-routes-and-paths.utils';

//  ** Constants
import { protectedRoutesMap } from 'modules/core/constants/routes/protected-routes.constants';
import { errorsRoutes } from 'modules/core/constants/routes/errors-routes.constants';
import { layoutsMap } from 'modules/core/constants/layouts-map.constants';

// ** Types
import { ILayout } from 'modules/core/types/layout.types';

const Admin: FC<RouteComponentProps> = () => {
  return (
    <Switch>
      {Object.keys(layoutsMap).map((layout) => {
        const currentLayout = layout as ILayout;

        const Layout: FC = layoutsMap[currentLayout];

        const { LayoutPaths, LayoutRoutes } = getRoutesAndPaths(
          currentLayout,
          protectedRoutesMap
        );

        return (
          <Route path={LayoutPaths} key={layout}>
            <Layout>
              <Switch>
                {LayoutRoutes.map(({ exact, component: Component, path }) => {
                  return (
                    <Route
                      key={path}
                      exact={exact}
                      path={path}
                      render={(props) => (
                        <LayoutWrapper>
                          <Suspense fallback={<Spinner />}>
                            <Component {...props} />
                          </Suspense>
                        </LayoutWrapper>
                      )}
                    />
                  );
                })}
                <Redirect to={errorsRoutes.error} />
              </Switch>
            </Layout>
          </Route>
        );
      })}

      <Redirect to={errorsRoutes.error} />
    </Switch>
  );
};

export default Admin;
