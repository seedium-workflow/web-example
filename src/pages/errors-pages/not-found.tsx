import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import errorImg from 'assets/images/pages/error.svg';

// ** Constants
import { protectedRoutes } from 'modules/core/constants/routes/protected-routes.constants';

import '@core/scss/base/pages/page-misc.scss';

const NotFound: React.FC = () => {
  return (
    <div className='misc-wrapper'>
      <div className='misc-inner p-2 p-sm-3'>
        <div className='w-100 text-center'>
          <h2 className='mb-1'>Page Not Found &#9888;&#65039;</h2>
          <p className='mb-2'>
            The requested URL was not found on this server.
          </p>
          <Button
            tag={Link}
            to={protectedRoutes.dashboard}
            color='primary'
            className='btn-sm-block mb-2'
          >
            Back to home
          </Button>
          <img className='img-fluid' src={errorImg} alt='Not authorized page' />
        </div>
      </div>
    </div>
  );
};

export default NotFound;
