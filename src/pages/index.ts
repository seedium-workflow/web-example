import React, { lazy } from 'react';
import { RouteComponentProps } from 'react-router-dom';

// Protected Pages
const Dashboard: React.LazyExoticComponent<React.FC<RouteComponentProps>> =
  lazy(() => import('./protected-pages/dashboard'));

const Admin: React.LazyExoticComponent<React.FC<RouteComponentProps>> = lazy(
  () => import('./protected-pages/admin')
);

// Errors Pages
const AccessDenied: React.LazyExoticComponent<React.FC<RouteComponentProps>> =
  lazy(() => import('./errors-pages/access-denied'));

const NotFound: React.LazyExoticComponent<React.FC<RouteComponentProps>> = lazy(
  () => import('./errors-pages/not-found')
);

// Auth Pages
const Auth: React.LazyExoticComponent<React.FC<RouteComponentProps>> = lazy(
  () => import('./auth-pages/auth')
);
const Login: React.LazyExoticComponent<React.FC<RouteComponentProps>> = lazy(
  () => import('./auth-pages/login')
);

const Register: React.LazyExoticComponent<React.FC<RouteComponentProps>> = lazy(
  () => import('./auth-pages/register')
);

const ForgotPassword: React.LazyExoticComponent<React.FC<RouteComponentProps>> =
  lazy(() => import('./auth-pages/forgot-password'));

const ResetPassword: React.LazyExoticComponent<React.FC<RouteComponentProps>> =
  lazy(() => import('./auth-pages/reset-password'));

const Auth0Callback: React.LazyExoticComponent<React.FC<RouteComponentProps>> =
  lazy(() => import('./auth-pages/auth0-callback'));

const VerifyEmail: React.LazyExoticComponent<React.FC<RouteComponentProps>> =
  lazy(() => import('./auth-pages/verify-email'));

export {
  Admin,
  Dashboard,
  AccessDenied,
  NotFound,
  Login,
  Register,
  ForgotPassword,
  ResetPassword,
  Auth0Callback,
  VerifyEmail,
  Auth,
};
