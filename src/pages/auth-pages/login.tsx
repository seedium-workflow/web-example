import { FC, useCallback } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Auth0Error } from 'auth0-js';
import { CardTitle, CardText, Form, FormGroup, Label } from 'reactstrap';

// ** Components
import {
  Button,
  InputPasswordToggle,
  InputText,
  Checkbox,
  SingleFormWrapper,
} from 'modules/core/components';

// ** Types
import { LoginCredentialsType } from 'modules/auth/types/login.types';

// ** Hooks
import { useBoolean } from 'modules/core/hooks/use-boolean';

// ** Schemas
import { loginSchema } from 'modules/auth/schemas/login.schema';

// ** Utils
import { showToast } from 'modules/core/utils/toast.utils';
import {
  addToLocalStorage,
  getFromLocalStorage,
} from 'modules/core/utils/storage.utils';

// ** Services
import { auth0Service } from 'modules/auth/services/auth0.services';

// ** Constants
import { authRoutes } from 'modules/core/constants/routes/auth-routes.constants';

const Login: FC = () => {
  const isRememberAuthorizationStorage = getFromLocalStorage(
    'isRememberAuthorization'
  );

  const [isLoadingLogin, setIsLoadingLogin] = useBoolean(false);
  const [isRememberAuthorization, setIsRememberAuthorization] = useBoolean(
    isRememberAuthorizationStorage === null
      ? true
      : isRememberAuthorizationStorage
  );

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginCredentialsType>({
    defaultValues: { email: '', password: '' },
    resolver: yupResolver(loginSchema),
  });

  const onSubmit = useCallback(
    async (data: LoginCredentialsType) => {
      try {
        setIsLoadingLogin.on();
        addToLocalStorage('isRememberAuthorization', isRememberAuthorization);
        await auth0Service.signIn(data);
      } catch (err) {
        const error = err as Auth0Error;

        setIsLoadingLogin.off();
        showToast('error', <>{error.description}</>);
      }
    },
    [setIsLoadingLogin, isRememberAuthorization]
  );

  const handleRememberMe = useCallback(() => {
    setIsRememberAuthorization.toggle();
    addToLocalStorage('isRememberAuthorization', !isRememberAuthorization);
  }, [setIsRememberAuthorization, isRememberAuthorization]);

  return (
    <SingleFormWrapper>
      <CardTitle tag='h4' className='mb-1'>
        Welcome Back! 👋
      </CardTitle>
      <CardText className='mb-2'>Please log in to your account</CardText>
      <Form className='auth-login-form mt-2' onSubmit={handleSubmit(onSubmit)}>
        <InputText
          label='Email'
          id='email'
          type='text'
          placeholder='Enter your email'
          invalid={!!errors.email?.message}
          feedbackText={errors.email?.message}
          {...register('email')}
        />
        <FormGroup>
          <div className='d-flex justify-content-between'>
            <Label className='form-label' for='password'>
              Password
            </Label>
            <Link to={authRoutes.forgotPassword}>
              <small>Forgot Password?</small>
            </Link>
          </div>
          <InputPasswordToggle
            id='password'
            invalid={!!errors.password?.message}
            feedbackText={errors.password?.message}
            {...register('password')}
          />
        </FormGroup>
        <FormGroup>
          <Checkbox
            checked={isRememberAuthorization}
            id='remember-me'
            label='Remember Me'
            onChange={handleRememberMe}
          />
        </FormGroup>
        <Button type='submit' color='primary' disabled={isLoadingLogin} block>
          Login
        </Button>
      </Form>
      <p className='text-center mt-2'>
        <span className='me-25'>New on our platform? </span>
        <Link to={authRoutes.register}>
          <span>Create an account</span>
        </Link>
      </p>
    </SingleFormWrapper>
  );
};

export default Login;
