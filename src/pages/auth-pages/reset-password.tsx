import React from 'react';
import { ChevronLeft } from 'react-feather';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Link } from 'react-router-dom';
import { CardTitle, CardText, Form, FormGroup } from 'reactstrap';

// ** Components
import {
  Button,
  InputPasswordToggle,
  SingleFormWrapper,
} from 'modules/core/components';

// ** Hooks
import { useBoolean } from 'modules/core/hooks/use-boolean';

// ** Schemas
import { resetPasswordSchema } from 'modules/auth/schemas/reset-password.schema';

// ** Types
import { ResetPasswordType } from 'modules/auth/types/reset-password.types';

// ** Constants
import { authRoutes } from 'modules/core/constants/routes/auth-routes.constants';

const ResetPassword: React.FC = () => {
  const [isLoading, setIsLoading] = useBoolean(false);

  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<ResetPasswordType>({
    defaultValues: { newPassword: '', confirmPassword: '' },
    resolver: yupResolver(resetPasswordSchema),
  });

  const onSubmit = (): void => {
    setIsLoading.on();
  };

  return (
    <SingleFormWrapper>
      <CardTitle tag='h4' className='mb-1'>
        Reset Password 🔒
      </CardTitle>
      <CardText className='mb-2'>
        Your new password must be different from previously used passwords
      </CardText>
      <Form
        className='auth-reset-password-form mt-2'
        onSubmit={handleSubmit(onSubmit)}
      >
        <FormGroup>
          <InputPasswordToggle
            className='input-group-merge'
            label='New Password'
            id='newPassword'
            invalid={!!errors.newPassword?.message}
            feedbackText={errors.newPassword?.message}
            {...register('newPassword')}
          />
        </FormGroup>
        <FormGroup>
          <InputPasswordToggle
            className='input-group-merge'
            label='Confirm Password'
            invalid={!!errors.confirmPassword?.message}
            feedbackText={errors.confirmPassword?.message}
            id='confirmPassword'
            {...register('confirmPassword')}
          />
        </FormGroup>
        <Button color='primary' block type='submit' disabled={isLoading}>
          Set New Password
        </Button>
      </Form>
      <p className='text-center mt-2'>
        <Link to={authRoutes.login}>
          <ChevronLeft className='mr-25' size={14} />
          <span className='align-middle'>Back to login</span>
        </Link>
      </p>
    </SingleFormWrapper>
  );
};

export default ResetPassword;
