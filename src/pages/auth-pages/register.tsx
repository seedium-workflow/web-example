import { FC, useCallback } from 'react';
import { Auth0Error } from 'auth0-js';
import { Link, RouteComponentProps } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Form, CardTitle, FormGroup } from 'reactstrap';

// ** Components
import {
  InputText,
  Button,
  InputPasswordToggle,
  Checkbox,
  SingleFormWrapper,
} from 'modules/core/components';

// ** Hooks
import { useBoolean } from 'modules/core/hooks/use-boolean';

// ** Services
import { auth0Service } from 'modules/auth/services/auth0.services';

// ** Utils
import { showToast } from 'modules/core/utils/toast.utils';
import { parseFullName } from 'modules/core/utils/parse-full-name.utils';

// ** Types
import {
  RegistrationFormValuesType,
  Auth0RegisterDataType,
} from 'modules/auth/types/auth0-register.types';

// ** Schemas
import { registerSchema } from 'modules/auth/schemas/register.schema';

// ** Constants
import { REGISTER_INITIAL_VALUES } from 'modules/auth/constants/register-initial-values.constants';
import { authRoutes } from 'modules/core/constants/routes/auth-routes.constants';

const Register: FC<RouteComponentProps> = () => {
  const [isRegistering, setIsRegistering] = useBoolean(false);

  const onSubmit = useCallback(
    async (data: RegistrationFormValuesType) => {
      setIsRegistering.on();

      const { first_name, last_name } = parseFullName(data.username);

      const actualData: Auth0RegisterDataType = {
        password: data.password,
        email: data.email,
        given_name: first_name,
        family_name: last_name,
      };

      try {
        await auth0Service.signUp(actualData);
      } catch (err) {
        const error = err as Auth0Error;

        setIsRegistering.off();
        showToast('error', <>{error.description}</>);
      }
    },
    [setIsRegistering]
  );

  const {
    handleSubmit,
    register,
    control,
    formState: { errors },
  } = useForm<RegistrationFormValuesType>({
    defaultValues: REGISTER_INITIAL_VALUES,
    resolver: yupResolver(registerSchema),
  });

  return (
    <SingleFormWrapper>
      <CardTitle tag='h4' className='mb-1 text-center'>
        Start your journey now! 😍
      </CardTitle>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <InputText
          label='Name'
          id='username'
          placeholder='Enter name'
          invalid={!!errors.username?.message}
          feedbackText={errors.username?.message}
          {...register('username')}
        />
        <InputText
          label='Email'
          id='email'
          placeholder='Enter email'
          invalid={!!errors.email?.message}
          feedbackText={errors.email?.message}
          {...register('email')}
        />
        <FormGroup>
          <InputPasswordToggle
            label='Password'
            id='password'
            invalid={!!errors.password?.message}
            feedbackText={errors.password?.message}
            {...register('password')}
          />
        </FormGroup>
        <FormGroup className='d-flex'>
          <Controller
            name='isAcceptedTerms'
            control={control}
            render={({ field: { onChange, value } }) => (
              <Checkbox
                id='isAcceptedTerms'
                checked={value}
                onChange={onChange}
                className='checkboxMarginRight'
                label='I agree to'
                invalid={!!errors.isAcceptedTerms?.message}
              />
            )}
          />
          <a
            // TODO: add actual link to privacy terms
            href='https://google.com'
            target='_blank'
            rel='noreferrer noopener'
          >
            privacy policy & terms
          </a>
        </FormGroup>

        <Button type='submit' block color='primary' disabled={isRegistering}>
          Sign up
        </Button>
        <p className='text-center mt-2'>
          <span className='mr-25'>Already have an account?</span>
          <Link to={authRoutes.login}>
            <span>Sign in instead</span>
          </Link>
        </p>
      </Form>
    </SingleFormWrapper>
  );
};

export default Register;
