import { FC, Suspense } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

// ** Utils
import { getRoutesAndPaths } from 'modules/core/utils/get-routes-and-paths.utils';

// ** Types
import { ILayout } from 'modules/core/types/layout.types';

// ** Components
import { AnimatedWrapper } from 'modules/core/components';

// ** Constants
import { authRoutesMap } from 'modules/core/constants/routes/auth-routes.constants';
import { layoutsMap } from 'modules/core/constants/layouts-map.constants';
import { errorsRoutes } from 'modules/core/constants/routes/errors-routes.constants';

const Auth: FC = () => {
  return (
    <Switch>
      {Object.keys(layoutsMap).map((layout) => {
        const currentLayout = layout as ILayout;

        const Layout: FC = layoutsMap[currentLayout];

        const { LayoutPaths, LayoutRoutes } = getRoutesAndPaths(
          currentLayout,
          authRoutesMap
        );

        return (
          <Route path={LayoutPaths} key={layout}>
            <Layout>
              <Switch>
                {LayoutRoutes.map(({ exact, component: Component, path }) => {
                  return (
                    <Route
                      key={path}
                      exact={exact}
                      path={path}
                      render={(props) => (
                        <Suspense fallback={null}>
                          <AnimatedWrapper>
                            <Component {...props} />
                          </AnimatedWrapper>
                        </Suspense>
                      )}
                    />
                  );
                })}
                <Redirect to={errorsRoutes.error} />
              </Switch>
            </Layout>
          </Route>
        );
      })}
      <Redirect to={errorsRoutes.error} />
    </Switch>
  );
};

export default Auth;
